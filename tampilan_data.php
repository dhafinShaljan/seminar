<?php  
require 'functions_seminar.php';
$seminar = query("SELECT * FROM pendaftaran");


// tombol cari ditekan
if( isset($_POST["cari"]) ) {
	$seminar = cari($_POST["keyword"]);
}


?>

<!DOCTYPE html>
<html>
<head>
	<title>Halaman Admin</title>
	<style type="text/css">
		body {
			background-color: #16A886;
			margin: 0;
			font-family: Helvetica;
			color: white;
			line-height: 30px;
		}
		.table {
			margin-top: 15px;
			border-collapse: collapse;
			width: 100%;
		}

		.table th {
			background-color: #088389;
			padding: 10px 15px;
			color: white;
		}

		.table td {
			padding: 10px 15px;
			color: #393939;
		}

		.table tr:nth-child(even) {
			background-color: white;
		}

		.table tr:nth-child(odd) {
			background-color: #dfdfdf;
		}
		a{
			text-decoration: none;
		}
		a:hover{
			color: red;
		}

		input[type="text"]{
			outline: none;
			padding: 8px 15px;
			background-color: #e8e8e8;
			font-family: Helvetica;
			border: solid 3px #e8e8e8;
			border-radius: 15px;
			transition: all 0.5s;
		}

		input[type="text"]:focus{
			background-color: white;
			border: solid 3px #0B8389;
			transition: all 0.5s;
		}

		button {
		  background-color: #36a5f4;
		  color: white;
		  padding: 12px 17px;
		  border-radius: 5px;
		  margin: 8px 0;
		  border: none;
		  cursor: pointer;
		}

		button:hover {
		  opacity: 0.8;
		}
	</style>
</head>
<body>

	<center>

	<h1>Daftar Data Pendaftar</h1>

	<a href="pendaftaran.php">Tambah Data Pendaftar</a>
	<br><br>

	<form action="" method="post">
		
		<input type="text" name="keyword" size="40" autofocus placeholder="Masukkan Keyword Pencarian..." autocomplete="off">
		<button type="submit" name="cari">Cari!</button>

	</form>

	<br>

	<table class="table">
		
		<tr>
			<th>No.</th>
			<th>Aksi</th>
			<th>Nama</th>
			<th>Email</th>
			<th>Nomor Telepon</th>
			<th>Tempat Lahir</th>
			<th>Tanggal Lahir</th>
			<th>Usia</th>
			<th>Asal</th>
			<th>Status</th>
			<th>Jenis Instansi</th>
			<th>Nama Instansi</th>
		</tr>

		<?php $i = 1; ?>
		<?php foreach ($seminar as $row) : ?>
		<tr>
			<td><?= $i ?></td>
			<td>
				<a href="edit_data.php?id=<?= $row["id"]; ?>">Edit</a> |
				<a href="hapus_data.php?id=<?= $row["id"]; ?>" onclick="return confirm('Apakah Anda yakin ingin menghapus?');">Hapus</a>
			</td>
			<td><?= $row["nama"]; ?></td>
			<td><?= $row["email"]; ?></td>
			<td><?= $row["no_telp"]; ?></td>
			<td><?= $row["tempat_lahir"]; ?></td>
			<td><?= $row["tgl_lahir"]; ?></td>
			<td><?= $row["usia"]; ?></td>
			<td><?= $row["asal"]; ?></td>
			<td><?= $row["status"]; ?></td>
			<td><?= $row["jenis_instansi"]; ?></td>
			<td><?= $row["nama_instansi"]; ?></td>
		</tr>
		<?php $i++; ?>
		<?php endforeach; ?>

	</table>

	</center>

</body>
</html>