<?php 
require 'functions_seminar.php';


// cek apakah tombol submit sudah ditekan atau belum
if ( isset($_POST["submit"]) ) {


	// cek apakah data berhasil di tambahkan atau tidak
	if( tambah($_POST) > 0 ) {
		echo "
			<script>
				alert('data berhasil ditambahkan!');
				document.location.href = 'tampilan_data.php';
			</script>
		";
	} else {
		echo "
			<script>
				alert('data gagal ditambahkan!');
				document.location.href = 'tampilan_data.php';
			</script>
		";
		echo mysqli_error($conn);
	}
}


?>


<!DOCTYPE html>
<html>
<head>
	<title>Form Pendaftaran Seminar Hacker</title>
	<style type="text/css">

		body{
			background-image: url(1.jpg);
			background-size: 110%;
			color: #edebeb;
			
		}

		h1, h2{
			font-family: "montserratff", helvetica, arial;
		}

		fieldset{
			width: 350px;
		}

		ul li{
			list-style: none;
			margin-top: 5px;
			margin-right: 32.5px;
			padding: 5px;
		}

		input{
			outline: none;
			padding: 5px 10px;
			background-color: #e8e8e8;
			font-family: Helvetica;
			border: solid 3px #e8e8e8;
			border-radius: 5px;
			transition: all 0.5s;
		}

		input:focus{
			background-color: white;
			border: solid 3px #0B8389;
			transition: all 0.5s;
		}


		label{
			display: block;
			font-family: Helvetica;
		}


		button {
			padding: 10px 20px;
			color: white; 
			border-radius: 10px;
			border: none;
			margin-top: 15px;
			font-weight: bold;

		}

		button[type="submit"]{
			background-color: #0B8389;
			border-bottom: solid 2px #036a6f;
		}

		button[type="reset"]{
			background-color: #d12f2f;
			border-bottom: solid 2px #af1f1f;
		}

		button:hover{
			opacity: 0.8;
		}

		@keyframes fadeIn{

			from {
				opacity: 0;
			}

			to {
				opacity: 1;
			}
		}
	</style>
</head>
<body>
	<center>
	<h1>Form  Pendaftaran Seminar Hacker</h1>
	<h2>"Security and hacking "</h2>

	<form action="" method="post">
		<fieldset width="500">
		<ul>
			<li>
				<label for="nama">Nama :</label>
				<input type="text" name="nama" id="nama" required>
			</li>
			<li>
				<label for="email">Email :</label>
				<input type="email" name="email" id="email" required>
			</li>
			<li>
				<label for="no_telp">No Telp :</label>
				<input type="phone" name="no_telp" id="no_telp">
			</li>
			<li>
				<label for="tempat_lahir">Tempat Lahir :</label>
				<input type="text" name="tempat_lahir" id="tempat_lahir">
			</li>
			<li>
				<label for="tgl_lahir">Tanggal Lahir :</label>
				<input type="date" name="tgl_lahir" id="tgl_lahir">
			</li>
			<li>
				<label for="usia">Usia :</label>
				<input type="number" name="usia" id="usia" min="17" max="50">
			</li>
			<li>
				<label for="asal">Asal :</label>
				<input type="text" name="asal" id="asal">
			</li>
			<li>
				<label for="status">Status :</label>
				<input type="radio" name="status" id="status" value="Menikah"> Menikah
				<input type="radio" name="status" id="status" value="Belum Menikah"> Belum Menikah
			</li>
			<li>
				<label for="jenis_instansi">Jenis Instansi :</label>
				<input type="radio" name="jenis_instansi" id="jenis_instansi" value="Badan Khusus">Badan Khusus 
				<input type="radio" name="jenis_instansi" id="jenis_instansi" value="Negara">Negara
				<input type="radio" name="jenis_instansi" id="jenis_instansi" value="Swasta">Swasta
				<input type="radio" name="jenis_instansi" id="jenis_instansi" value="Lainnya">Lainnya
			</li>
			<li>
				<label for="nama_instansi">Nama Instansi :</label>
				<input type="text" name="nama_instansi" id="nama_instansi" required>
			</li>
			<li>
				<button type="submit" name="submit">Daftar</button>
				<button type="reset">Reset</button>
			</li>
		</ul>
		</fieldset>
	</form>
	</center>
</body>
</html>